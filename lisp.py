#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re, os, string, subprocess, copy, binascii, sys
import tempfile
from argparse import ArgumentParser


class LispObject:
	""" Just some object that can be eval'd, e.g. function result or a number """
	pass


'''
    Function classes
'''


class Function(LispObject):
	""" A lambda function definition class """

	def __init__(self, params, code):
		self.params = params
		self.code = code

	def __repr__(self):
		return '%s (%s) {%s}' % (self.__class__.__name__, ','.join(str(x) for x in self.params), self.code)


class FunctionVarArgs(Function):
	""" A variant of function that accepts variable arguments """
	pass


class FunctionCall(LispObject):
	""" A function call class """

	def __init__(self, name, params):
		self.name = name
		self.params = params

	def __repr__(self):
		return '%s %s(%s)' % (self.__class__.__name__, self.name, ','.join(str(x) for x in self.params))


class AppliedFunctionCall(FunctionCall):
	pass


class LibcCall(LispObject):
	"""
	A native C++ function call class,
	requires proper headers to be included in prolog
	"""

	def __init__(self, signature, name, params):
		self.signature = signature
		self.name = name
		self.params = params

	def __repr__(self):
		return '%s libc_%s_%s(%s)' % (
			self.__class__.__name__, self.name, self.signature, ','.join(str(x) for x in self.params))


class IfCall(LispObject):
	""" If-else call class, else is not optional """

	def __init__(self, cond, act1, act2):
		self.cond = cond
		self.act1 = act1
		self.act2 = act2

	def __repr__(self):
		return '%s %s(%s,%s)' % (self.__class__.__name__, self.cond, self.act1, self.act2)


class RecurCall(LispObject):
	""" Optimized tail recursion call class """

	def __init__(self, params):
		self.params = params

	def __repr__(self):
		return '%s (%s)' % (self.__class__.__name__, ','.join(str(x) for x in self.params))


class LetCall(LispObject):
	""" Local variable definition call class """

	def __init__(self, params, code):
		self.params = params
		self.code = code

	def __repr__(self):
		return '%s (%s) {%s}' % (self.__class__.__name__, ','.join(str(x) for x in self.params), self.code)


'''
    Thread-related classes
'''


class DelayCall(LispObject):
	""" Thread creation call class """

	def __init__(self, code):
		self.code = code

	def __repr__(self):
		return '%s {%s}' % (self.__class__.__name__, self.code)


class ForceCall(LispObject):
	""" Thread join call class """

	def __init__(self, param):
		self.param = param

	def __repr__(self):
		return '%s (%s)' % (self.__class__.__name__, self.param)


'''
    Exception-related classes
'''

'''
    As there is no exception handling in Scheme, we are using our own syntax
    
    Exception handling:
        (try (e)
            (Main function body)
            (Error handler, will have "e" variable defined)
        )
    Exception throwing:
        (raise e)
        e is an object of any type
'''


class TryCall(LispObject):
	""" Exception handling try call class """

	def __init__(self, param, code, handler):
		self.param = param
		self.code = code
		self.handler = handler

	def __repr__(self):
		return '%s %s(%s,%s)' % (self.__class__.__name__, self.param, self.code, self.handler)


class RaiseCall(LispObject):
	""" Exception handling raise call class """

	def __init__(self, param):
		self.param = param

	def __repr__(self):
		return '%s (%s)' % (self.__class__.__name__, self.param)


'''
    Macro-related classes
'''


class QuoteCall(LispObject):
	"""
		Quote object without evaluation
		Not actually a call, but is processed the same way
	"""

	def __init__(self, params):
		self.params = params

	def __repr__(self):
		return '%s (%s)' % (self.__class__.__name__, self.params)


class MacroDefinition(LispObject):
	""" Macro definition class """

	def __init__(self, name, params, code):
		self.name = name
		self.params = params
		self.code = code

	def __repr__(self):
		return '%s/%s (%s) {%s}' % (
			self.__class__.__name__, self.name, ','.join(str(x) for x in self.params), self.code)


class MacroExpand(LispObject):
	""" Macro expansion class """

	def __init__(self, code):
		self.code = code

	def __repr__(self):
		return '%s (%s)' % (self.__class__.__name__, self.code)


class MacroCall(LispObject):
	""" A macro call class """

	def __init__(self, name, params):
		self.name = name
		self.params = params

	def __repr__(self):
		return '%s %s(%s)' % (self.__class__.__name__, self.name, ','.join(str(x) for x in self.params))


'''
    Constant classes
'''


class Constant(LispObject):
	""" Constant (of any type) class """

	def __init__(self, value):
		self.value = value

	def __repr__(self):
		return '%s %s' % (self.__class__.__name__, self.value)


class ConstFloat(Constant):
	""" Float constant class """
	pass


class ConstInt(Constant):
	""" Integer constant class """
	pass


class ConstStr(Constant):
	""" String constant class """
	pass


'''
    Language object classes
'''


class Symbol(LispObject):
	""" Symbol definition class """

	def __init__(self, name, value):
		self.value = value
		self.name = name

	def __repr__(self):
		return '%s %s=%s' % (self.__class__.__name__, self.name, self.value)


class SymbolRef(LispObject):
	""" Symbol reference class """

	def __init__(self, name):
		self.name = name

	def __repr__(self):
		return '%s %s' % (self.__class__.__name__, self.name)


class Sequence(LispObject):
	""" Sequence class """

	def __init__(self, value):
		self.value = value

	def __repr__(self):
		return '%s %s' % (self.__class__.__name__, self.value)

	def __getitem__(self, key):
		return self.value[key]

	def __setitem__(self, key, v):
		self.value[key] = v


class ArgSequence(LispObject):
	""" Argument sequence, cannot be modified by recursive parser """

	def __init__(self, value):
		self.value = value

	def __repr__(self):
		return '%s %s' % (self.__class__.__name__, self.value)

	def __getitem__(self, key):
		return self.value[key]

	def __setitem__(self, key, v):
		self.value[key] = v


def isbracket(c):
	""" Bracket of any kind """
	return c == '(' or c == ')' or c == '[' or c == ']'


def tokenizer(s):
	ret = []
	# Remove comments
	s = re.sub(re.compile('^\s*(;.*?)$', re.MULTILINE), '', s)
	while len(s) > 0:
		# Remove whitespaces
		s = re.sub('^(\s+)', '', s)
		if len(s) == 0:
			break
		if isbracket(s[0]):
			ret += [s[0]]
			s = s[1:]
			continue
		# https://stackoverflow.com/questions/2039795/regular-expression-for-a-string-literal-in-flex-lex
		isstr = re.search('^("([^\\\\"]|\\\\.)*?")', s)
		if isstr is not None:
			isstr = isstr.group(1)
			ret += [isstr]
			s = s[len(isstr):]
			continue
		token = re.search('^(.*?)([\\(|\\)|\\[|\\]|\s]+|$)', s)
		if token is None:
			raise RuntimeError("Unhandled token")
		token = token.group(1)
		ret += [token]
		s = s[len(token):]
	return ret


def matching_bracket_id(tokens, startcount=1):
	count = startcount
	for x in range(len(tokens)):
		if tokens[x] == '(':
			count += 1
		if tokens[x] == ')':
			count -= 1
		if count == 0:
			return x


''' Returns an array of parsed tokens '''


def recursive_parser(tokens):
	if len(tokens) == 0:
		return []
	if len(tokens) == 1:
		token = tokens[0]
		# Number
		try:
			return [ConstInt(int(token))]
		except:
			pass
		# Floating number
		try:
			return [ConstFloat(float(token))]
		except:
			pass
		# String
		if token.startswith('"'):
			return [ConstStr(token[1:-1])]
		return [SymbolRef(token)]
	if tokens[0] == '(':
		end = matching_bracket_id(tokens[1:]) + 1
		return [Sequence(recursive_parser(tokens[1:end]))] + recursive_parser(tokens[end + 1:])
	else:
		ret = []
		for id, token in enumerate(tokens):
			if token == '(':				
				ret += recursive_parser(tokens[id:])
				break
			ret += recursive_parser([token])
		return ret


def builtin_def(fref):
	if len(fref.params) > 2:
		raise RuntimeError("Too many arguments to def")
	if not isinstance(fref.params[0], SymbolRef):
		raise RuntimeError("First argument to def is not a proper name")
	return Symbol(fref.params[0].name, fref.params[1])


def builtin_lambda(fref):
	if len(fref.params) > 2:
		raise RuntimeError("Too many arguments to lambda")
	if isinstance(fref.params[0], ArgSequence):
		return Function(fref.params[0].value, fref.params[1])
	# Var-args function detected
	elif isinstance(fref.params[0], SymbolRef):
		return FunctionVarArgs([fref.params[0]], fref.params[1])
	else:
		raise RuntimeError("First argument to lambda is not a list of args")


def builtin_defun(fref):
	fref_lambda = FunctionCall('lambda', fref.params[1:])
	fref_def = FunctionCall('def', [fref.params[0]] + [builtin_lambda(fref_lambda)])
	return builtin_def(fref_def)


def builtin_libc(fref):
	return LibcCall(fref.params[1], fref.params[0].name, fref.params[2:])


def builtin_recur(fref):
	return RecurCall(fref.params)


def builtin_let(fref):
	if len(fref.params) != 2:
		raise RuntimeError("Let accepts two arguments")
	return LetCall(fref.params[0].value, fref.params[1])


def builtin_quote(fref):
	if len(fref.params) != 1:
		raise RuntimeError("Quote can quote only one argument")
	return QuoteCall(fref.params[0])


def builtin_if(fref):
	if len(fref.params) != 3:
		raise RuntimeError("Wrong argument count to if")
	return IfCall(fref.params[0], fref.params[1], fref.params[2])


def builtin_defmacro(fref):
	if len(fref.params) != 3:
		raise RuntimeError("Wrong argument count to defmacro")
	return MacroDefinition(fref.params[0], fref.params[1].value, fref.params[2])


# Better call this on a copy of object
def expand_in_quotes(fref, globalstate):
	if isinstance(fref, QuoteCall):
		fref.params = expand_in_quotes(fref.params, globalstate)
		return fref
	elif isinstance(fref, ArgSequence) or isinstance(fref, Sequence):
		if len(fref.value) > 0:
			namez = [x.name.name for x in globalstate['macros']]
			if isinstance(fref.value[0], SymbolRef) and fref.value[0].name in namez:
				# So, we've got a macro
				macro = globalstate['macros'][namez.index(fref.value[0].name)]

				def purge_argseq(fref):
					if isinstance(fref, ArgSequence):
						return purge_argseq(Sequence(fref.value))
					elif isinstance(fref, Sequence):
						for i in range(len(fref.value)):
							fref.value[i] = purge_argseq(fref.value[i])
					return fref

				if len(fref.value[1:]) != len(macro.params):
					raise RuntimeError("Trying to expand macro with wrong number of args")
				replacement = copy.deepcopy(macro.code)
				replacement = purge_argseq(replacement)
				varz = macro.params
				replz = fref.value[1:]
				replacement = Sequence([SymbolRef('let'), Sequence(
					[Sequence([v, Sequence([SymbolRef('quote'), r])]) for v, r in zip(varz, replz)]), replacement])
				replacement = Sequence([SymbolRef('repr'), replacement])
				textout = runlisp_batch([replacement])
				# Parse macro-expanded output as text
				ptokens = text_to_tokens(textout)
				# So we finally have the expanded (BUT NOT BUILTIN-PARSED!) form, assign it to the original
				if len(ptokens) != 1:
					raise RuntimeError("Our macro have expanded to somethind weird of length %s" % (len(ptokens),))
				return ptokens[0]
		for i, x in enumerate(fref.value):
			fref.value[i] = expand_in_quotes(x, globalstate)
		return fref
	else:
		return fref


def builtin_macroexpand(fref):
	if len(fref.params) != 1:
		raise RuntimeError("Wrong argument count to macroexpand")
	param = fref.params[0]
	return MacroExpand(param)


def builtin_delay(fref):
	if len(fref.params) != 1:
		raise RuntimeError("delay can accept only one argument")
	return DelayCall(fref.params[0])


def builtin_force(fref):
	if len(fref.params) != 1:
		raise RuntimeError("force can accept only one argument")
	return ForceCall(fref.params[0])


def builtin_try(fref):
	if len(fref.params) != 3:
		raise RuntimeError("Wrong argument count to try")
	if len(fref.params[0].value) > 1:
		raise RuntimeError("try accepts only one exception variable")
	return TryCall(fref.params[0].value[0], fref.params[1], fref.params[2])


def builtin_raise(fref):
	if len(fref.params) != 1:
		raise RuntimeError("raise can accept only one argument")
	return RaiseCall(fref.params[0])


def builtin_apply(fref):
	if len(fref.params) != 2:
		raise RuntimeError("apply accepts two arguments")
	return AppliedFunctionCall(fref.params[0].name, [fref.params[1]])


def builtin_parser(ptokens, globalstate):
	""" Returns an array of processed tokens with implemented builtins """
	for id, t in enumerate(ptokens):
		# We are not parsing constants and symbolreferences
		if isinstance(t, Sequence):
			# Check for special constructions that accept sequences as arguments, not functions
			if isinstance(t[0], SymbolRef):
				fname = t[0].name
				if fname == "lambda":
					if isinstance(t[1], Sequence):
						t[1] = ArgSequence(t[1].value)
				elif fname == "defun":
					if isinstance(t[2], Sequence):
						t[2] = ArgSequence(t[2].value)
				elif fname == "let":
					if isinstance(t[1], Sequence):
						t[1] = ArgSequence(t[1].value)
						for x in t[1].value:
							if len(x.value) != 2:
								raise RuntimeError("Let accepts pairs only!")
							# Still have to process the executable part of let
							builtin_parser(x.value, globalstate)
					else:
						raise RuntimeError("Let accepts sequences only!")
				elif fname == "defmacro":
					macroname = t[1].name
					globalstate['defmacros'][macroname] = True
					if isinstance(t[2], Sequence):
						t[2] = ArgSequence(t[2].value)
					else:
						raise RuntimeError("Var-args macros are not allowed")
					if isinstance(t[3], Sequence):
						t[3] = ArgSequence(t[3].value)
				elif fname in globalstate['defmacros']:
					# All args will remain immutable
					for x in range(1, len(t.value)):
						if isinstance(t[x], Sequence):
							t[x] = ArgSequence(t[x].value)
				elif fname == "quote":
					if isinstance(t[1], Sequence):
						t[1] = ArgSequence(t[1].value)
				elif fname == "try":
					if isinstance(t[1], Sequence):
						t[1] = ArgSequence(t[1].value)
			builtin_parser(t.value, globalstate)
			ptokens[id] = FunctionCall(t[0].name, t[1:])
			f = ptokens[id]
			if f.name == 'define':
				ptokens[id] = builtin_def(f)
			elif f.name == 'lambda':
				ptokens[id] = builtin_lambda(f)
			# Could be implemented with macros, but is written here for optimization
			elif f.name == 'defun':
				ptokens[id] = builtin_defun(f)
			elif f.name == 'libc':
				ptokens[id] = builtin_libc(f)
			elif f.name == 'if':
				ptokens[id] = builtin_if(f)
			elif f.name == 'recur':
				ptokens[id] = builtin_recur(f)
			elif f.name == 'let':
				ptokens[id] = builtin_let(f)
			elif f.name == 'quote':
				ptokens[id] = builtin_quote(f)
			elif f.name == 'defmacro':
				ptokens[id] = builtin_defmacro(f)
			elif f.name == 'macroexpand':
				ptokens[id] = builtin_macroexpand(f)
			elif f.name == 'delay':
				ptokens[id] = builtin_delay(f)
			elif f.name == 'force':
				ptokens[id] = builtin_force(f)
			elif f.name == 'try':
				ptokens[id] = builtin_try(f)
			elif f.name == 'raise':
				ptokens[id] = builtin_raise(f)
			elif f.name == 'apply':
				ptokens[id] = builtin_apply(f)
			elif f.name in globalstate['defmacros']:
				ptokens[id] = MacroCall(t[0].name, t[1:])


uniqctr = 0


def unique_provider():
	""" Provides unique names for lambdas and other anonymous objects """
	global uniqctr
	uniqctr += 1
	return 'skbk%s' % (uniqctr,)


def str2hex(x):
	if sys.version_info >= (3, 0):
		return binascii.hexlify(x.encode('utf-8')).decode('utf-8')
	return x.encode('hex')


def symname_filter(f):
	blacklist = ['not', 'read', 'const', 'and', 'false', 'true', 'or']
	if f in blacklist:
		return '__skobka_escape_' + f
	return ''.join(x if x in string.ascii_letters + '0123456789' else 'x' + str2hex(x) for x in f)


def inherit(ll, ret):
	decl = '\n'.join(x['decl'] for x in ll if 'decl' in x).strip('\n')
	impl = '\n'.join(x['impl'] for x in ll if 'impl' in x).strip('\n')
	if decl != '':
		decl += '\n'
	if impl != '':
		impl += '\n'
	ret['decl'] += decl
	ret['impl'] += impl


def codegen_quote(s):
	if isinstance(s, ArgSequence) or isinstance(s, Sequence):
		rt = [codegen_quote(x) for x in s.value]
		return 'new LispList(%s,(LispType**)(new void*[%s]{%s}))' % (len(rt), len(rt), ','.join(x for x in rt))
	elif isinstance(s, Constant):
		hackystring = codegen_token(s, {}, {})['decl']
		# a VERY hacky approach (but shares code)
		return hackystring[hackystring.index('=') + 1:-2]  # Remove ;\n
	elif isinstance(s, SymbolRef):
		return 'new LispSymbol("%s")' % s.name
	else:
		raise RuntimeError("Unknown type of quoted stuff")


'''
    Assumptions:
        All variables are the pointers (even numbers)
'''


def codegen_token(t, context, globalstate):
	"""
		Generates C language construction from the token
		Returns a dict that contains:
			decl - what to append in the declaration area of program
			val - a value of token
			impl - what to append in the function implementation area of program
	"""
	ret = {'decl': '', 'impl': ''}
	# We copy the context to edit it
	context = dict(context)
	if isinstance(t, Constant):
		# Constant handling
		rndname = 'var_' + unique_provider()
		type = None
		val = t.value
		if isinstance(t, ConstFloat):
			type = 'LispNumber'
		if isinstance(t, ConstInt):
			type = 'LispNumber'
		if isinstance(t, ConstStr):
			type = 'LispString'
			val = '"' + val + '"'
		if type is None:
			raise RuntimeError("Not implemented")
		ret['decl'] += type + '* ' + rndname + '=new ' + type + '(%s);' % val + '\n'
		ret['val'] = rndname
	elif isinstance(t, Function):
		lambdadef = '''
class lambda_%s: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_%s(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        '''		
		lambdaimp = '''
void* lambda_%s::call(LispList* __skobka_args)
{
    lambda_start:
	%s%s
    %s
    %s
}
        '''
		rndname = unique_provider()
		if isinstance(t, FunctionVarArgs):
			paras = 'LispList* %s=__skobka_args;\n' % (symname_filter(t.params[0].name),)
			cntcheck = '' # No check when function is var-args
		else:
			paras = ''.join(
				'void* %s=__skobka_args->val[%s];\n' % (symname_filter(x.name), i) for i, x in enumerate(t.params))
			cntcheck = 'if (!(%s == 0 && (__skobka_args == 0)) && (__skobka_args->size != %s))\n{\nprintf("Wrong number of parameters detected to the lambda_%s: %%d instead of %s\\n",__skobka_args->size);\n}\n' % (len(t.params),len(t.params),rndname,len(t.params))
		oldargs = context.get('args', [])
		ctxvars = ''.join('void* %s=__skobka_context->val[%s];\n' % (x, i) for i, x in enumerate(oldargs))
		context['args'] = context.get('args', []) + [symname_filter(x.name) for x in t.params]
		definition = lambdadef % (rndname, rndname) + ';\n'
		ll = codegen_token(t.code, context, globalstate)
		implementation = lambdaimp % (rndname, cntcheck, paras, ctxvars, 'return ' + ll['val'] + ';\n')
		inherit([ll], ret)
		ret['decl'] += definition
		ret['impl'] += implementation
		ret['val'] = 'new lambda_%s(new LispList(%s,(LispType**)(new void*[%s]{%s})))' % (
			rndname, len(oldargs), len(oldargs), ','.join(x for x in oldargs))
	elif isinstance(t, FunctionCall):
		ll = [codegen_token(x, context, globalstate) for x in t.params]
		inherit(ll, ret)
		if symname_filter(t.name) in prolog_functions:
			if isinstance(t, AppliedFunctionCall):			
				tmpnam = 'tmp_'+unique_provider()
				ret['val'] = '({LispList* %s = (LispList*)%s;' % (tmpnam,ll[0]['val']) + symname_filter(t.name) + '(' + ','.join('%s->val[%s]' % (tmpnam,i) for i in range(prolog_functions[symname_filter(t.name)])) + ');'+'})'
			else:
				# Builtin function call
				ret['val'] = symname_filter(t.name) + '(' + ','.join(x['val'] for x in ll) + ')'
		else:
			if isinstance(t, AppliedFunctionCall):
				ret['val'] = '((LispLambda*)' + symname_filter(t.name) + ')->call((LispList*)(%s))' % (ll[0]['val'],)
			else:
				# Var-args function call
				ret['val'] = '((LispLambda*)' + symname_filter(
					t.name) + ')->call(new LispList(%s,(LispType**)(new void*[%s]{%s})))' % (
					len(ll), len(ll), ','.join(x['val'] for x in ll))
	elif isinstance(t, RecurCall):
		# Can only be used inside lambdas
		ll = [codegen_token(x, context, globalstate) for x in t.params]
		inherit(ll, ret)
		ret['val'] = '({%s;goto lambda_start;(void*)0;})' % (
			';'.join('__skobka_args->val[%s]=(LispType*)%s' % (i, x['val']) for i, x in enumerate(ll)))
	elif isinstance(t, LetCall):
		nm = []
		ll = []
		for pair in t.params:
			name, val = pair
			name = symname_filter(name.name)
			cg = codegen_token(val, context, globalstate)
			nm += [name]
			ll += [cg]
		context['args'] = context.get('args', []) + nm
		cd = codegen_token(t.code, context, globalstate)
		inherit(ll + [cd], ret)
		ret['val'] = '({%s;%s;})' % (
			';'.join('void* %s=%s;' % (nm[i], ll[i]['val']) for i in range(len(ll))), cd['val'])
	elif isinstance(t, QuoteCall):
		# Feels bad, man
		if isinstance(t.params, ArgSequence):
			# List as argument
			ret['val'] = codegen_quote(t.params)
		else:
			ret['val'] = codegen_quote(t.params)
	elif isinstance(t, LibcCall):
		ll = [codegen_token(x, context, globalstate) for x in t.params]
		# Signature has a format of d for integer, s for string, f for float, v for void
		# p for pointer,
		# caps letters are pointers casted to the appropriate type (e.g. S -> char*)
		# Return value is written in the end after a colon
		if not isinstance(t.signature, ConstStr):
			raise RuntimeError("Signature should be constant string")
		p1, p2 = t.signature.value.split(':')
		if len(p1) != len(ll):
			raise RuntimeError("Signature doesn't match arguments")
		rx = t.name + '('
		argzcast = []
		for p, x in zip(p1, ll):
			if p == 'd':
				argzcast += ['((LispNumber*)' + x['val'] + ')->asint()']
			elif p == 'f':
				argzcast += ['((LispNumber*)' + x['val'] + ')->asfp()']
			elif p == 's':
				argzcast += ['((LispString*)' + x['val'] + ')->val']
			elif p == 'p':
				argzcast += ['((LispNativePointer*)' + x['val'] + ')->val']
			elif p == 'S':
				argzcast += ['(char*)(((LispNativePointer*)' + x['val'] + ')->val)']
			else:
				raise RuntimeError("Not implemented")
		callmask = '%s'
		if p2 == 'd' or p2 == 'f':
			callmask = 'new LispNumber(%s)'
		elif p2 == 's':
			callmask = 'new LispString(%s)'
		elif p2 == 'p':
			callmask = 'new LispNativePointer(%s)'
		# Return 0 in case the return value is void
		elif p2 == 'v':
			callmask = '((%s),(void*)0)'
		inherit(ll, ret)
		ret['val'] = callmask % (t.name + '(' + ','.join(argzcast) + ')')
	elif isinstance(t, IfCall):
		ll = [codegen_token(x, context, globalstate) for x in [t.cond, t.act1, t.act2]]
		inherit(ll, ret)
		ret['val'] = '(__skobka_tologic(' + ll[0]['val'] + ')?' + ll[1]['val'] + ':' + ll[2]['val'] + ')'
	elif isinstance(t, Symbol):
		tmp = codegen_token(t.value, context, globalstate)
		inherit([tmp], ret)
		# define now has retval!
		ret['decl'] += 'void* ' + symname_filter(t.name) + ';'
		ret['val'] = symname_filter(t.name) + '=(void*)(' + tmp['val'] + ')'
	elif isinstance(t, SymbolRef):
		ret['val'] = symname_filter(t.name)
	elif isinstance(t, MacroDefinition):
		globalstate['macros'] += [t]
	elif isinstance(t, MacroExpand):
		unexpanded = expand_in_quotes(copy.deepcopy(t.code), globalstate)
		return codegen_token(unexpanded, context, globalstate)
	elif isinstance(t, MacroCall):
		unexpanded = expand_in_quotes(Sequence([SymbolRef(t.name)] + copy.deepcopy(t.params)), globalstate)
		lsttoparse = [unexpanded]
		builtin_parser(lsttoparse, globalstate)
		return codegen_token(lsttoparse[0], context, globalstate)
	elif isinstance(t, DelayCall):
		delaydef = '''
class promise_%s: public LispPromise
{
    public:    
    promise_%s(LispLambda* threadfunc) { LispPromise::threadfunc=threadfunc; startThread(); };    
};
        '''
		rndname = unique_provider()
		ll = codegen_token(Function([], t.code), context, globalstate)
		definition = delaydef % (rndname, rndname) + ';\n'
		inherit([ll], ret)
		ret['decl'] += definition
		ret['val'] = 'new promise_%s(%s)' % (rndname, ll['val'])
	elif isinstance(t, ForceCall):
		ll = codegen_token(t.param, context, globalstate)
		inherit([ll], ret)
		ret['val'] = '((LispPromise*)(%s))->join()' % (ll['val'],)
	elif isinstance(t, TryCall):
		catchname = symname_filter(t.param.name)
		tryimpl = '''
        ({
            void* ret;
            try
            {
                ret=%s;
            }
            catch (void* %s)
            {
                ret=%s;
            }
            ret;
        })
        '''
		ll = [codegen_token(t.code, context, globalstate)]
		# Now add exception variable to context
		context['args'] = context.get('args', []) + [catchname]
		ll += [codegen_token(t.handler, context, globalstate)]
		inherit(ll, ret)
		ret['val'] = tryimpl % (ll[0]['val'], catchname, ll[1]['val'])
	elif isinstance(t, RaiseCall):
		ll = codegen_token(t.param, context, globalstate)
		inherit([ll], ret)
		ret['val'] = '({throw (void*)(%s);(void*)0;})' % (ll['val'],)
	else:
		print('Warning: %s not implemented' % (t,))
	return ret


def codegen_list(ptokens, globalstate):
	""" Main code generator, accepts processed tokens and globalstate dict """
	title = prolog + '\n'
	funcs = ''
	code = 'int main()\n{\n'
	for p in ptokens:
		rt = codegen_token(p, {}, globalstate)
		title += rt.get('decl', '')
		funcs += rt.get('impl', '')
		if 'val' in rt:
			code += rt['val'] + ';\n'  # Any top-level expression should have ; after it
	code += 'return 0;}\n'
	return title + '\n' + funcs + '\n' + code + '\n' + epilog


def text_to_tokens(text):
	""" Translates text to tokens """
	tokens = tokenizer(text)
	return recursive_parser(tokens)


def translate_tokens(tokens, fout):
	""" Translates tokens to C++ source code """
	globalstate = {'macros': [], 'defmacros': {}}
	ptokens = copy.deepcopy(lispprolog_tokens) + tokens
	builtin_parser(ptokens, globalstate)
	srcgen = codegen_list(ptokens, globalstate)
	fo = open(fout, 'w')
	fo.write(srcgen)
	fo.close()


def translate(code, fout):
	""" Translates text to C++ source code """
	return translate_tokens(text_to_tokens(code), fout)


def buildbinary(code, cxxflags, fout):
	""" Builds binary from code """
	translate(code, fout + '.cpp')
	subprocess.check_call(['g++', fout + '.cpp', '-w', '-lpthread', '-std=c++11'] + cxxflags.split(' ') + ['-o', fout])
	os.unlink(fout + '.cpp')


def buildbinary_tokens(tokens, fout):
	""" Builds binary from tokens, used for macros """
	translate_tokens(tokens, fout + '.cpp')
	subprocess.check_call(['g++', fout + '.cpp', '-w', '-lpthread', '-std=c++11', '-o', fout])
	os.unlink(fout + '.cpp')


def runlisp_batch(tokens):
	""" Runs lisp from tokens, used for macros """
	tmp = tempfile.mkstemp()
	os.close(tmp[0])
	# Hack for Cygwin
	os.unlink(tmp[1])
	buildbinary_tokens(tokens, tmp[1])
	ret = subprocess.check_output(tmp[1]).decode('utf-8')
	os.unlink(tmp[1])
	return ret


# Initialization
SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
prolog = open(SCRIPT_DIR + '/prolog.cpp').read()
tmp_prolfuncs = re.findall('PROLOG\((\d+)\) (\w*)', prolog)
prolog_functions = {}
for p in tmp_prolfuncs:
	prolog_functions[p[1]]=int(p[0])
epilog = ''
lispprolog_tokens = text_to_tokens(open(SCRIPT_DIR + '/prolog.lisp').read())

if __name__ == "__main__":
	defaultbinary = 'a.out'
	parser = ArgumentParser()
	parser.add_argument("source", type=str, help="read source code from file SOURCE", metavar="SOURCE", default=None)
	parser.add_argument("-o", "--output", dest="output", type=str, help="write binary to file OUTPUT", metavar="OUTPUT",
						default=None)
	parser.add_argument("-f", "--flags", dest="flags", type=str, help="pass CXXFLAGS to gcc, space-separated",
						metavar="CXXFLAGS", default='-Os -s')
	parser.add_argument("-e", "--eval", dest="eval", help="run program instead of building", default=False,
						action="store_true")
	parser.add_argument("-s", "--translate", dest="translate", help="translate program instead of building",
						default=False, action="store_true")
	options = parser.parse_args()
	if options.eval and options.output != None:
		parser.error("cannot use eval and output at the same time")
	if options.eval and options.translate:
		parser.error("cannot use eval and translate at the same time")
	if options.eval:
		tmp = tempfile.mkstemp()
		os.close(tmp[0])
		# Hack for Cygwin
		os.unlink(tmp[1])
		buildbinary(open(options.source).read(), options.flags, tmp[1])
		subprocess.check_call(tmp[1])
		os.unlink(tmp[1])
	else:
		if options.output == None:
			output = defaultbinary
		else:
			output = options.output
		if options.translate:
			translate(open(options.source).read(), output + '.cpp')
		else:
			buildbinary(open(options.source).read(), options.flags, output)
