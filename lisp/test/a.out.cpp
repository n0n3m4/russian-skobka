#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

// This will be used in Python for parsing the source
#define PROLOG

class LispType
{
	public:
	virtual ~LispType() {}
};

class LispNumber: public LispType
{	
	double fpval;
	int val;
	public:
	bool isfp;
	LispNumber(double val): fpval(val) {isfp=1;}
	LispNumber(int val): val(val) {isfp=0;}
	int asint()
	{
		if (!isfp) return val;
		return (int)(val+0.5);
	}
	
	double asfp()
	{
		if (isfp) return fpval;
		return val;
	}
	
	LispNumber* add(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp()+o->asfp());
		return new LispNumber(asint()+o->asint());
	}
	
	LispNumber* sub(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp()-o->asfp());
		return new LispNumber(asint()-o->asint());
	}
	
	LispNumber* mul(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp()*o->asfp());
		return new LispNumber(asint()*o->asint());
	}
	
	LispNumber* div(LispNumber* o)
	{
		// Always FP
		return new LispNumber(asfp()/o->asfp());		
	}
	
	LispNumber* le(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp() < o->asfp());
		return new LispNumber(asint() < o->asint());
	}
	
	LispNumber* eq(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp() == o->asfp());
		return new LispNumber(asint() == o->asint());
	}
};

class LispString: public LispType
{
	public:
	const char* val;
	LispString(const char* val): val(val) {};	
	LispString* add(LispString* o)
	{
		char* t=(char*)malloc(strlen(val)+strlen(o->val)+1);
		t[0]=0;
		strcat(t,val);
		strcat(t,o->val);
		return new LispString((const char*)t);
	}		
	LispNumber* le(LispString* o)
	{		
		return new LispNumber(strcmp(val,o->val) < 0);
	}
	
	LispNumber* eq(LispString* o)
	{
		return new LispNumber(strcmp(val,o->val) == 0);
	}
};

class LispList: public LispType
{
	public:
	int size;
	LispType** val;
	LispList(int size,LispType** _val): size(size)
	{
		val=(LispType**)malloc(size*sizeof(LispType*));
		memcpy(val,_val,size*sizeof(LispType*));
	};
	LispList* add(LispType* addval)
	{
		LispType* tmp[size+1];
		memcpy(tmp,val,size*sizeof(LispType*));
		tmp[size]=addval;
		return new LispList(size+1,tmp);
	}
	LispList* pre(LispType* addval)
	{
		LispType* tmp[size+1];
		memcpy(tmp+1,val,size*sizeof(LispType*));
		tmp[0]=addval;
		return new LispList(size+1,tmp);
	}
	LispList* skip()
	{
		return new LispList(size-1,val+1);
	}
};

class LispSymbol: public LispType
{
	public:
	const char* name;
	LispSymbol(const char* name): name(name) {};
};

class LispNativePointer: public LispType
{
	public:
	void* val;
	LispNativePointer(void* val): val(val) {};
};

class LispLambda: public LispType
{
	public:	
	virtual void* call(LispList* args) {};
};

// Linux-only stuff, didn't bother supporting Windows

#include <pthread.h>
void* promise_threadfunc(void*);
class LispPromise: public LispType
{	
	pthread_t thread;
	void* retval=0;
	public:
	int isrunning()
	{
		return retval==0;
	}
	LispLambda* threadfunc;
	void startThread()
	{
		pthread_create(&thread, NULL, &promise_threadfunc, this);
	}
	void* join()
	{
		if (!retval)
			pthread_join(thread,&retval);
		return retval;
	}
};

void* promise_threadfunc(void* self)
{
	return ((LispPromise*)self)->threadfunc->call(NULL);
}

#define num_str_op(name,impl)\
	void* name(void* a,void* b)\
	{\
		LispString* sa=dynamic_cast<LispString*>((LispType*)a);\
		if (sa == 0)\
			return ((LispNumber*)a)->impl((LispNumber*)b);\
		else\
			return ((LispString*)a)->impl((LispString*)b);\
	}
	
#define num_op(name,impl)\
	void* name(void* a,void* b)\
	{\
		return ((LispNumber*)a)->impl((LispNumber*)b);\
	}

// Base binary operations, all are implemented in C++
num_str_op(PROLOG x2b,add)
num_op(PROLOG x2d,sub)
num_op(PROLOG x2a,mul)
num_op(PROLOG x2f,div)
// Comparison operations, most to be implemented in Lisp
num_str_op(PROLOG x3c,le)
num_str_op(PROLOG x3d,eq)
// Binary logical operations
int __skobka_tologic(void* e)
{
	return ((LispNumber*)(e))->asint();
}
void* __skobka_fromlogic(int b)
{
	return new LispNumber(b);
}
void* PROLOG __skobka_escape_not(void* e)
{
	return __skobka_fromlogic(!__skobka_tologic(e));
}
void* PROLOG binx2dor(void* a,void* b)
{
	return __skobka_fromlogic(__skobka_tologic(a) || __skobka_tologic(b));
}
void* PROLOG binx2dand(void* a,void* b)
{
	return __skobka_fromlogic(__skobka_tologic(a) && __skobka_tologic(b));
}
// Type-checking operations
#define istype(name,type)\
	void* name(void* x)\
	{\
		return __skobka_fromlogic(dynamic_cast<type*>((LispType*)x)!=0);\
	}
istype(PROLOG numberx3f,	LispNumber)
istype(PROLOG stringx3f,	LispString)
istype(PROLOG listx3f,		LispList)
istype(PROLOG symbolx3f,	LispSymbol)
istype(PROLOG promisex3f,	LispPromise)
// Cannot be implemented with macro
void* PROLOG floatx3f(void* x)
{
	LispNumber* p=dynamic_cast<LispNumber*>((LispType*)x);
	if (p == 0)
		return __skobka_fromlogic(0);
	return __skobka_fromlogic(p->isfp);
}
// Promise-related stuff
void* PROLOG promisex2drunningx3f(void* x)
{
	return __skobka_fromlogic(((LispPromise*)x)->isrunning());
}
// Pointer-related stuff
void* PROLOG getx2dmemory(void* x)
{
	return new LispNativePointer(malloc(((LispNumber*)(x))->asint()));
}
// List-related stuff
void* PROLOG car(void* x)
{
	return ((LispList*)x)->val[0];
}
void* PROLOG cdr(void* x)
{
	return ((LispList*)x)->skip();
}
void* PROLOG nullx3f(void* x)
{	
	return __skobka_fromlogic(((LispList*)x)->size == 0);
}
void* PROLOG cons(void* a,void* b)
{	
	LispList* l=dynamic_cast<LispList*>((LispType*)b);
	// Second argument is a list, just prepend a value there
	if (l)
		return l->pre((LispType*)a);
	else
		return new LispList(2,(LispType**)(new void*[2]{a,b}));
}
void* PROLOG append(void* a,void* b)
{	
	return ((LispList*)b)->add((LispType*)a);	
}
// Symbol-related stuff
void* PROLOG symbolx2dname(void* x)
{
	return new LispString(((LispSymbol*)x)->name);
}
class lambda_skbk1: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk1(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* x3cx3d;class lambda_skbk2: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk2(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* x3e;class lambda_skbk3: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk3(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* x3ex3d;class lambda_skbk4: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk4(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* intx3f;class lambda_skbk5: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk5(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* list;class lambda_skbk6: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk6(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* last;class lambda_skbk7: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk7(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* begin;LispNumber* var_skbk9=new LispNumber(0);

LispString* var_skbk10=new LispString(" ");

LispNumber* var_skbk11=new LispNumber(0);

class lambda_skbk8: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk8(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* displayx2dlistx2drec;LispString* var_skbk13=new LispString("(");


LispString* var_skbk14=new LispString(")");

class lambda_skbk12: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk12(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* displayx2dlist;LispString* var_skbk16=new LispString("%d");

LispString* var_skbk17=new LispString("%f");

LispString* var_skbk18=new LispString("%s");

LispString* var_skbk19=new LispString("NOT IMPLEMENTED\n");

class lambda_skbk15: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk15(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* display;LispNumber* var_skbk21=new LispNumber(0);

LispString* var_skbk22=new LispString(" ");

LispNumber* var_skbk23=new LispNumber(0);

class lambda_skbk20: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk20(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* reprx2dlistx2drec;LispString* var_skbk25=new LispString("(");


LispString* var_skbk26=new LispString(")");

class lambda_skbk24: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk24(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* reprx2dlist;LispString* var_skbk28=new LispString("\"");


LispString* var_skbk29=new LispString("\"");

LispString* var_skbk30=new LispString("REPR NOT IMPLEMENTED!\n");

class lambda_skbk27: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk27(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* repr;LispString* var_skbk32=new LispString("\n");

class lambda_skbk31: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk31(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* newline;LispNumber* var_skbk34=new LispNumber(4096);

class lambda_skbk33: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk33(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* __skobka_escape_read;class lambda_skbk35: public LispLambda
{
    public:
    LispList* __skobka_context;
    lambda_skbk35(LispList* context): __skobka_context(context) {};    
    void* call(LispList* args);
}
        ;
void* readnum;LispString* var_skbk36=new LispString("Hello world!\n");

void* lambda_skbk1::call(LispList* __skobka_args)
{
    lambda_start:
    void* a=__skobka_args->val[0];
void* b=__skobka_args->val[1];

    
    return binx2dor(x3c(a,b),x3d(a,b));

}
        
void* lambda_skbk2::call(LispList* __skobka_args)
{
    lambda_start:
    void* a=__skobka_args->val[0];
void* b=__skobka_args->val[1];

    
    return __skobka_escape_not(((LispLambda*)x3cx3d)->call(new LispList(2,(LispType**)(new void*[2]{a,b}))));

}
        
void* lambda_skbk3::call(LispList* __skobka_args)
{
    lambda_start:
    void* a=__skobka_args->val[0];
void* b=__skobka_args->val[1];

    
    return __skobka_escape_not(x3c(a,b));

}
        
void* lambda_skbk4::call(LispList* __skobka_args)
{
    lambda_start:
    void* a=__skobka_args->val[0];

    
    return binx2dand(numberx3f(a),__skobka_escape_not(floatx3f(a)));

}
        
void* lambda_skbk5::call(LispList* __skobka_args)
{
    lambda_start:
    LispList* args=__skobka_args;

    
    return args;

}
        
void* lambda_skbk6::call(LispList* __skobka_args)
{
    lambda_start:
    void* l=__skobka_args->val[0];

    
    return (__skobka_tologic(nullx3f(cdr(l)))?car(l):((LispLambda*)last)->call(new LispList(1,(LispType**)(new void*[1]{cdr(l)}))));

}
        
void* lambda_skbk7::call(LispList* __skobka_args)
{
    lambda_start:
    LispList* args=__skobka_args;

    
    return ((LispLambda*)last)->call(new LispList(1,(LispType**)(new void*[1]{args})));

}
        
void* lambda_skbk8::call(LispList* __skobka_args)
{
    lambda_start:
    void* l=__skobka_args->val[0];

    
    return (__skobka_tologic(nullx3f(l))?var_skbk9:((LispLambda*)begin)->call(new LispList(3,(LispType**)(new void*[3]{({void* tmp=car(l);;((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{tmp})));}),(__skobka_tologic(__skobka_escape_not(nullx3f(cdr(l))))?((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk10}))):var_skbk11),((LispLambda*)displayx2dlistx2drec)->call(new LispList(1,(LispType**)(new void*[1]{cdr(l)})))}))));

}
        
void* lambda_skbk12::call(LispList* __skobka_args)
{
    lambda_start:
    void* l=__skobka_args->val[0];

    
    return ((LispLambda*)begin)->call(new LispList(3,(LispType**)(new void*[3]{((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk13}))),((LispLambda*)displayx2dlistx2drec)->call(new LispList(1,(LispType**)(new void*[1]{l}))),((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk14})))})));

}
        
void* lambda_skbk15::call(LispList* __skobka_args)
{
    lambda_start:
    void* a=__skobka_args->val[0];

    
    return (__skobka_tologic(((LispLambda*)intx3f)->call(new LispList(1,(LispType**)(new void*[1]{a}))))?((printf(((LispString*)var_skbk16)->val,((LispNumber*)a)->asint())),(void*)0):(__skobka_tologic(floatx3f(a))?((printf(((LispString*)var_skbk17)->val,((LispNumber*)a)->asfp())),(void*)0):(__skobka_tologic(stringx3f(a))?((printf(((LispString*)var_skbk18)->val,((LispString*)a)->val)),(void*)0):(__skobka_tologic(listx3f(a))?((LispLambda*)displayx2dlist)->call(new LispList(1,(LispType**)(new void*[1]{a}))):((printf(((LispString*)var_skbk19)->val)),(void*)0)))));

}
        
void* lambda_skbk20::call(LispList* __skobka_args)
{
    lambda_start:
    void* l=__skobka_args->val[0];

    
    return (__skobka_tologic(nullx3f(l))?var_skbk21:((LispLambda*)begin)->call(new LispList(3,(LispType**)(new void*[3]{((LispLambda*)repr)->call(new LispList(1,(LispType**)(new void*[1]{car(l)}))),(__skobka_tologic(__skobka_escape_not(nullx3f(cdr(l))))?((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk22}))):var_skbk23),((LispLambda*)reprx2dlistx2drec)->call(new LispList(1,(LispType**)(new void*[1]{cdr(l)})))}))));

}
        
void* lambda_skbk24::call(LispList* __skobka_args)
{
    lambda_start:
    void* l=__skobka_args->val[0];

    
    return ((LispLambda*)begin)->call(new LispList(3,(LispType**)(new void*[3]{((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk25}))),((LispLambda*)reprx2dlistx2drec)->call(new LispList(1,(LispType**)(new void*[1]{l}))),((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk26})))})));

}
        
void* lambda_skbk27::call(LispList* __skobka_args)
{
    lambda_start:
    void* a=__skobka_args->val[0];

    
    return (__skobka_tologic(binx2dor(((LispLambda*)intx3f)->call(new LispList(1,(LispType**)(new void*[1]{a}))),floatx3f(a)))?((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{a}))):(__skobka_tologic(stringx3f(a))?((LispLambda*)begin)->call(new LispList(3,(LispType**)(new void*[3]{((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk28}))),((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{a}))),((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk29})))}))):(__skobka_tologic(listx3f(a))?((LispLambda*)reprx2dlist)->call(new LispList(1,(LispType**)(new void*[1]{a}))):(__skobka_tologic(symbolx3f(a))?((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{symbolx2dname(a)}))):((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk30})))))));

}
        
void* lambda_skbk31::call(LispList* __skobka_args)
{
    lambda_start:
    
    
    return ((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk32})));

}
        
void* lambda_skbk33::call(LispList* __skobka_args)
{
    lambda_start:
    
    
    return new LispString(gets((char*)(((LispNativePointer*)getx2dmemory(var_skbk34))->val)));

}
        
void* lambda_skbk35::call(LispList* __skobka_args)
{
    lambda_start:
    
    
    return new LispNumber(atoi(((LispString*)((LispLambda*)__skobka_escape_read)->call(new LispList(0,(LispType**)(new void*[0]{}))))->val));

}
        

int main()
{
x3cx3d=(void*)(new lambda_skbk1(new LispList(0,(LispType**)(new void*[0]{}))));
x3e=(void*)(new lambda_skbk2(new LispList(0,(LispType**)(new void*[0]{}))));
x3ex3d=(void*)(new lambda_skbk3(new LispList(0,(LispType**)(new void*[0]{}))));
intx3f=(void*)(new lambda_skbk4(new LispList(0,(LispType**)(new void*[0]{}))));
list=(void*)(new lambda_skbk5(new LispList(0,(LispType**)(new void*[0]{}))));
last=(void*)(new lambda_skbk6(new LispList(0,(LispType**)(new void*[0]{}))));
begin=(void*)(new lambda_skbk7(new LispList(0,(LispType**)(new void*[0]{}))));
displayx2dlistx2drec=(void*)(new lambda_skbk8(new LispList(0,(LispType**)(new void*[0]{}))));
displayx2dlist=(void*)(new lambda_skbk12(new LispList(0,(LispType**)(new void*[0]{}))));
display=(void*)(new lambda_skbk15(new LispList(0,(LispType**)(new void*[0]{}))));
reprx2dlistx2drec=(void*)(new lambda_skbk20(new LispList(0,(LispType**)(new void*[0]{}))));
reprx2dlist=(void*)(new lambda_skbk24(new LispList(0,(LispType**)(new void*[0]{}))));
repr=(void*)(new lambda_skbk27(new LispList(0,(LispType**)(new void*[0]{}))));
newline=(void*)(new lambda_skbk31(new LispList(0,(LispType**)(new void*[0]{}))));
__skobka_escape_read=(void*)(new lambda_skbk33(new LispList(0,(LispType**)(new void*[0]{}))));
readnum=(void*)(new lambda_skbk35(new LispList(0,(LispType**)(new void*[0]{}))));
((LispLambda*)display)->call(new LispList(1,(LispType**)(new void*[1]{var_skbk36})));
return 0;}

