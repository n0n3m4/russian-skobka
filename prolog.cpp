#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

// This will be used in Python for parsing the source
// x is the argument count
#define PROLOG(x)

class LispType
{
	public:
	virtual ~LispType() {}
};

class LispNumber: public LispType
{	
	double fpval;
	int val;
	public:
	bool isfp;
	LispNumber(double val): fpval(val) {isfp=1;}
	LispNumber(int val): val(val) {isfp=0;}
	int asint()
	{
		if (!isfp) return val;
		return (int)(val+0.5);
	}
	
	double asfp()
	{
		if (isfp) return fpval;
		return val;
	}
	
	LispNumber* add(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp()+o->asfp());
		return new LispNumber(asint()+o->asint());
	}
	
	LispNumber* sub(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp()-o->asfp());
		return new LispNumber(asint()-o->asint());
	}
	
	LispNumber* mul(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp()*o->asfp());
		return new LispNumber(asint()*o->asint());
	}
	
	LispNumber* div(LispNumber* o)
	{
		// Always FP
		return new LispNumber(asfp()/o->asfp());		
	}
	
	LispNumber* le(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp() < o->asfp());
		return new LispNumber(asint() < o->asint());
	}
	
	LispNumber* eq(LispNumber* o)
	{
		if (isfp || o->isfp)
			return new LispNumber(asfp() == o->asfp());
		return new LispNumber(asint() == o->asint());
	}
};

class LispString: public LispType
{
	public:
	const char* val;
	LispString(const char* val): val(val) {};	
	LispString* add(LispString* o)
	{
		char* t=(char*)malloc(strlen(val)+strlen(o->val)+1);
		t[0]=0;
		strcat(t,val);
		strcat(t,o->val);
		return new LispString((const char*)t);
	}		
	LispNumber* le(LispString* o)
	{		
		return new LispNumber(strcmp(val,o->val) < 0);
	}
	
	LispNumber* eq(LispString* o)
	{
		return new LispNumber(strcmp(val,o->val) == 0);
	}
};

class LispList: public LispType
{
	public:
	int size;
	LispType** val;
	LispList(int size,LispType** _val): size(size)
	{
		val=(LispType**)malloc(size*sizeof(LispType*));
		memcpy(val,_val,size*sizeof(LispType*));
	};
	LispList* add(LispType* addval)
	{
		LispType* tmp[size+1];
		memcpy(tmp,val,size*sizeof(LispType*));
		tmp[size]=addval;
		return new LispList(size+1,tmp);
	}
	LispList* pre(LispType* addval)
	{
		LispType* tmp[size+1];
		memcpy(tmp+1,val,size*sizeof(LispType*));
		tmp[0]=addval;
		return new LispList(size+1,tmp);
	}
	LispList* concat(LispList* other)
	{
		LispType* tmp[size+other->size];
		memcpy(tmp,val,size*sizeof(LispType*));
		memcpy(tmp+size,other->val,other->size*sizeof(LispType*));		
		return new LispList(size+other->size,tmp);
	}
	LispList* skip()
	{
		return new LispList(size-1,val+1);
	}
};

class LispSymbol: public LispType
{
	public:
	const char* name;
	LispSymbol(const char* name): name(name) {};
};

class LispNativePointer: public LispType
{
	public:
	void* val;
	LispNativePointer(void* val): val(val) {};
};

class LispLambda: public LispType
{
	public:	
	virtual void* call(LispList* args) {};
};

// Linux-only stuff, didn't bother supporting Windows

#include <pthread.h>
void* promise_threadfunc(void*);
class LispPromise: public LispType
{	
	pthread_t thread;
	void* retval=0;
	public:
	int isrunning()
	{
		return retval==0;
	}
	LispLambda* threadfunc;
	void startThread()
	{
		pthread_create(&thread, NULL, &promise_threadfunc, this);
	}
	void* join()
	{
		if (!retval)
			pthread_join(thread,&retval);
		return retval;
	}
};

void* promise_threadfunc(void* self)
{
	return ((LispPromise*)self)->threadfunc->call(NULL);
}

#define num_str_op(name,impl)\
	void* name(void* a,void* b)\
	{\
		LispString* sa=dynamic_cast<LispString*>((LispType*)a);\
		if (sa == 0)\
			return ((LispNumber*)a)->impl((LispNumber*)b);\
		else\
			return ((LispString*)a)->impl((LispString*)b);\
	}
	
#define num_op(name,impl)\
	void* name(void* a,void* b)\
	{\
		return ((LispNumber*)a)->impl((LispNumber*)b);\
	}

// Base binary operations, all are implemented in C++
num_str_op(PROLOG(2) x2b,add)
num_op(PROLOG(2) x2d,sub)
num_op(PROLOG(2) x2a,mul)
num_op(PROLOG(2) x2f,div)
// Comparison operations, most to be implemented in Lisp
num_str_op(PROLOG(2) x3c,le)
num_str_op(PROLOG(2) x3d,eq)
// Binary logical operations
int __skobka_tologic(void* e)
{
	return ((LispNumber*)(e))->asint();
}
void* __skobka_fromlogic(int b)
{
	return new LispNumber(b);
}
void* PROLOG(1) __skobka_escape_not(void* e)
{
	return __skobka_fromlogic(!__skobka_tologic(e));
}
void* PROLOG(2) binx2dor(void* a,void* b)
{
	return __skobka_fromlogic(__skobka_tologic(a) || __skobka_tologic(b));
}
void* PROLOG(2) binx2dand(void* a,void* b)
{
	return __skobka_fromlogic(__skobka_tologic(a) && __skobka_tologic(b));
}
// Type-checking operations
#define istype(name,type)\
	void* name(void* x)\
	{\
		return __skobka_fromlogic(dynamic_cast<type*>((LispType*)x)!=0);\
	}
istype(PROLOG(1) numberx3f,	LispNumber)
istype(PROLOG(1) stringx3f,	LispString)
istype(PROLOG(1) listx3f,		LispList)
istype(PROLOG(1) symbolx3f,	LispSymbol)
istype(PROLOG(1) promisex3f,	LispPromise)
// Cannot be implemented with macro
void* PROLOG(1) floatx3f(void* x)
{
	LispNumber* p=dynamic_cast<LispNumber*>((LispType*)x);
	if (p == 0)
		return __skobka_fromlogic(0);
	return __skobka_fromlogic(p->isfp);
}
// Promise-related stuff
void* PROLOG(1) promisex2drunningx3f(void* x)
{
	return __skobka_fromlogic(((LispPromise*)x)->isrunning());
}
// Pointer-related stuff
void* PROLOG(1) getx2dmemory(void* x)
{
	return new LispNativePointer(malloc(((LispNumber*)(x))->asint()));
}
// List-related stuff
void* PROLOG(1) car(void* x)
{
	return ((LispList*)x)->val[0];
}
void* PROLOG(1) cdr(void* x)
{
	return ((LispList*)x)->skip();
}
void* PROLOG(1) nullx3f(void* x)
{	
	return __skobka_fromlogic(((LispList*)x)->size == 0);
}
void* PROLOG(2) cons(void* a,void* b)
{	
	LispList* l=dynamic_cast<LispList*>((LispType*)b);
	// Second argument is a list, just prepend a value there
	if (l)
		return l->pre((LispType*)a);
	else
		return new LispList(2,(LispType**)(new void*[2]{a,b}));
}
void* PROLOG(2) append(void* a,void* b)
{	
	return ((LispList*)b)->add((LispType*)a);	
}
void* PROLOG(2) binx2dconcat(void* a,void* b)
{
	return ((LispList*)a)->concat((LispList*)b);
}
// Symbol-related stuff
void* PROLOG(1) symbolx2dname(void* x)
{
	return new LispString(((LispSymbol*)x)->name);
}