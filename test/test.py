import unittest
import subprocess
import os


class BaseTestCase(unittest.TestCase):
	@staticmethod
	def get_file_content(file_path):
		return open(file_path).read()

	@staticmethod
	def test_out():
		return "./test.out"

	@staticmethod
	def rm(file_path):
		if os.path.exists(file_path):
			os.remove(file_path)

	@staticmethod
	def prepare():
		BaseTestCase.rm(EqualOutputTest.test_out())

	@staticmethod
	def get_output(command):
		return subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()

	@staticmethod
	def call(command):
		subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read()


class EqualOutputTest(BaseTestCase):
	def do_test(self, script, output):
		self.prepare()
		self.get_output("python lisp.py -o " + self.test_out() + " " + script)
		self.assertEqual(self.get_output(self.test_out()), output)


class OutputFlagTest(BaseTestCase):
	def test(self):
		self.prepare()
		self.call("python lisp.py -o " + self.test_out() + " test/lisp/simple_program.lisp")
		self.assertTrue(os.path.exists(self.test_out()))


class EvalFlagTest(BaseTestCase):
	def test(self):
		self.prepare()
		self.assertEqual(self.get_output("python lisp.py -e test/lisp/simple_program.lisp"), "Hello world!\n")


class SimpleProgramTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/simple_program.lisp", "Hello world!\n")


class AdditionProgramTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/addition.lisp", "42")


class StringAdditionProgramTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/str_addition.lisp", "part1part2")


class MathProgramTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/math.lisp", "8.333333")


class CommentProgramTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/comment.lisp", "Ok")


class FactProgramTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/fact.lisp", "3628800")


class Fact2ProgramTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/fact2.lisp", "3628800")


class ClosureProgramTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/closure.lisp", "11")


class BoolLogicTest(EqualOutputTest):
	def test(self):
		self.do_test("test/lisp/homework.lisp", "(t and (z and (x or y)))\n((x and z and t) or (y and z and t))")


if __name__ == '__main__':
	unittest.main()
