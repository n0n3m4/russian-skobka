; definition methods
; n-ary
(defun sym-and args (cons "and" args))
(defun sym-or args (cons "or" args))
; binary
(defun sym-impl (a b) (cons "impl" (list a b)))
; unary
(defun sym-not (arg) (cons "not" (list arg)))
; const (accepts true/false)
(defun const (val) (cons "const" (list val)))
; var (accepts keywords)
(defun sym-sym (varkw) (cons "var" (list varkw)))

; remove useless implications, step 1 of DNF construction
(defun remove-implications (expr)
  (if (list? expr)
    (if (= (nth expr 0) "impl")
      (remove-implications (sym-or (sym-not (nth expr 1)) (nth expr 2)))
      (map remove-implications expr)
    )
    expr
  )
)

; eliminate nots applied to something more complex than a var
; as well as nots inside nots, step 2 of DNF construction
(defun eliminate-nots (expr)
  (if (list? expr)
    (if (= (nth expr 0) "not")
      (let ((notexpr (nth expr 1)))
        (if (= (nth notexpr 0) "and") 
          (eliminate-nots (apply sym-or (map sym-not (drop 1 notexpr))))
          (if (= (nth notexpr 0) "or")
            (eliminate-nots (apply sym-and (map sym-not (drop 1 notexpr))))
            ; not-inside-not
            (if (= (nth notexpr 0) "not")
              (eliminate-nots (nth notexpr 1))
              expr
            )
          )
        )
      )
      (map eliminate-nots expr)
    )
    expr
  )
)

; Ensure "and" is top-level only in case its childs are never "or"
; step 3 of DNF construction
(defun dnfy (expr_in)
  (if (list? expr_in)
	(let ((expr (map dnfy expr_in) ))
		(if (= (nth expr 0) "and")
		  (let ((
				ors (filter (lambda (e) (and (list? e) (= (nth e 0) "or"))) (drop 1 expr))
				))
			(if (null? ors)
			  expr
			  ; ors array is not empty, feels bad man                    
			 (apply sym-or 
			  (mapcat 
				(lambda (orseq) (map (lambda (oritem)
					(apply sym-and
					  ; Current part of OR statement 
					  (cons oritem
					  ; All parts that are not current
					  (filter (lambda (x) (not (= x orseq))) (drop 1 expr))
					  )
					)
					)
					; drop "or"
					(drop 1 orseq)
					)
				) ors
			  )
			 )          
			)
		  )
		  ; Not "and", everything fine
		  expr
		)
	)
    expr_in
  )
)

; Join included statements
(defun joindnf (expr)
  (if (list? expr)
    (if (or (= (nth expr 0) "and") (= (nth expr 0) "or"))
	(let ((
			filterlambda (lambda (e) (and (list? e) (= (nth e 0) (nth expr 0)))) 
		))
      (let 
        (
			(splitted_true (filter filterlambda (drop 1 expr)))
			(splitted_false (filter (lambda (e) (not (filterlambda e))) (drop 1 expr)))
		)		
        (map joindnf (concat (cons (nth expr 0) (mapcat (lambda (x) (drop 1 x)) splitted_true)) splitted_false))
      )
     ) 
      (map joindnf expr)
    )
    expr
  )
)

; Combination of methods to get DNF
(defun dnf (expr)
  (joindnf (dnfy (eliminate-nots (remove-implications expr))))
)

; Methods required for Russian Skobka
(defun str x
	(let ((str_recfunc
		(lambda (acc curl)
			(if (null? curl)
				acc				
				(if (string? (car curl))
					(recur (+ acc (car curl)) (cdr curl))
					(recur (+ acc "TODO") (cdr curl))
				)				
			)
		)
		))
		(str_recfunc "" x)
	)
)

(defun join (del l)
	(+ (car l) (apply str (map (lambda (x) (+ del x)) (cdr l))))
)

; Return expression in a readable (not RPN) form
(defun non-rpn (expr)
  (if (list? expr)
    (if (or (= (nth expr 0) "or") (= (nth expr 0) "and") (= (nth expr 0) "impl"))
      (str "(" (join (str " " (nth expr 0) " ") (map non-rpn (drop 1 expr))) ")")
      (if (= (nth expr 0) "not")
        (str "!" (apply str (map non-rpn (drop 1 expr))))
        (if (= (nth expr 0) "var")
          (str (nth expr 1))
          (if (= (nth expr 0) "const")
            (str (nth expr 1))
            (map non-rpn expr)
          )
        )
      )      
    )
    (str expr)
  )
)

(define eq0 (sym-and (sym-sym "t") (sym-and (sym-sym "z") (sym-or (sym-sym "x") (sym-sym "y")))))
(define eqdnf (dnf eq0))
(display (non-rpn eq0))
(newline)
(display (non-rpn eqdnf))